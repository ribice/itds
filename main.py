import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt
import string
import knn_utils
import re

bosnian_stop_words = [
    "i",
    "a",
    "ili",
    "ali",
    "pa",
    "te",
    "da",
    "u",
    "po",
    "na",
    "za",
    "sa",
    "o",
    "do",
    "od",
    "pre",
    "preko",
    "pod",
    "iz",
    "uz",
    "oko",
    "nad",
    "među",
    "s",
    "t",
    "j",
    "k",
    "b",
    "n",
    "m",
    "d",
    "što",
    "koji",
    "koja",
    "koje",
    "kojim",
    "kojima",
    "čiji",
    "čija",
    "čije",
    "čijim",
    "čijima",
    "ovaj",
    "ovog",
    "ovoj",
    "ovome",
    "ovi",
    "ove",
    "ova",
    "ovima",
    "onaj",
    "onog",
    "onoj",
    "onome",
    "oni",
    "one",
    "ona",
    "onima",
    "ko",
    "šta",
    "gdje",
    "kuda",
    "kako",
    "zašto",
    "čemu",
    "neki",
    "nekog",
    "nekoj",
    "nekome",
    "nekih",
    "nekima",
    "taj",
    "tog",
    "toj",
    "tome",
    "ti",
    "te",
    "ta",
    "tima",
    "ko",
    "kog",
    "koj",
    "kome",
    "koga",
    "čiji",
    "čijeg",
    "čijoj",
    "čijem",
    "čije",
    "čijih",
    "čijim",
    "sam",
    "si",
    "je",
    "smo",
    "ste",
    "su",
    "biti",
    "bio",
    "bila",
    "bili",
    "bilo",
    "bit",
    "biće",
    "htjeti",
    "htio",
    "htjela",
    "htjeli",
    "htjelo",
    "htjet",
    "htjeti",
    "morati",
    "morao",
    "morala",
    "morali",
    "moralo",
    "morat",
    "moći",
    "mogao",
    "mogla",
    "mogli",
    "moglo",
    "možda",
    "valjda",
    "trebati",
    "trebao",
    "trebala",
    "trebali",
    "trebalo",
    "trebat",
    "imati",
    "imao",
    "imala",
    "imali",
    "imalo",
    "imat",
    "željeti",
    "želio",
    "željela",
    "željeli",
    "željelo",
    "željet",
    "moj",
    "moja",
    "moje",
    "moji",
    "moje",
    "moja",
    "tvoj",
    "tvoja",
    "tvoje",
    "tvoji",
    "tvoje",
    "tvoja",
    "njegov",
    "njegova",
    "njegovo",
    "njegovi",
    "njegove",
    "njegova",
    "njen",
    "njena",
    "njeno",
    "njeni",
    "njene",
    "njena",
    "naš",
    "naša",
    "naše",
    "naši",
    "naše",
    "naša",
    "vaš",
    "vaša",
    "vaše",
    "vaši",
    "vaše",
    "vaša",
    "svaki",
    "svakog",
    "svakoj",
    "svakome",
    "svaki",
    "svake",
    "svaka",
    "svakim",
    "svakih",
    "svakima",
    "bilo koji",
    "bilo kojeg",
    "bilo kojoj",
    "bilo kojem",
    "bilo koji",
    "bilo koje",
    "bilo koja",
    "bilo kojim",
    "bilo kojih",
    "bilo kojima",
    "svi",
    "sve",
    "svega",
    "svemu",
    "sve",
    "sva",
    "svim",
    "svih",
    "svima",
    "niko",
    "ništa",
    "ničega",
    "ničemu",
    "nikakav",
    "nikakog",
    "nikakoj",
    "nikakom",
    "nikakav",
    "nikakva",
    "nikakvo",
    "nikakvi",
    "nikakve",
    "nikakva",
    "nikakvim",
    "nikakvih",
    "nikakvima",
    "se",
    "će",
    "bi",
    "km",
    "se",
    "to",
    "is",
    "ga",
    "ne",
    "to",
]


def remove_stop_words(text):
    words = text.split()
    filtered_words = [word for word in words if word.lower() not in bosnian_stop_words]
    return " ".join(filtered_words)


column_names = [
    "title",
    "link",
    "article_class",
    "article_class_name",
    "num_of_comments",
    "num_of_shares",
    "picture_path",
    "text",
]


filepath = "klix.csv"
df = pd.read_csv(filepath)


# Cut to 10000 for testing
df = df.head(100000)

print("Preprocessing text...")
df = df.apply(lambda x: x.str.strip() if x.dtype == "object" else x)
df["text"] = df["text"].str.lower()
df["text"] = df["text"].apply(lambda x: x[:1000])
df["text"] = df["text"].apply(
    lambda x: re.sub(r"[{}]".format(string.punctuation), "", x)
)
df["text"] = df["text"].apply(remove_stop_words)
print("Finished preprocessing text")


def plot_elbow_silhouette(sse, silhouette_coefficients, max_k):
    """
    Plot the Elbow Method and Silhouette Coefficients for determining the optimal number of clusters.

    :param sse: List of SSE values for each number of clusters.
    :param silhouette_coefficients: List of silhouette coefficient values for each number of clusters.
    :param max_k: The maximum number of clusters tested.
    """
    # Range of number of clusters
    k_range = range(2, max_k + 1)

    # Elbow Method Plot
    plt.figure(figsize=(14, 6))

    plt.subplot(1, 2, 1)
    plt.plot(k_range, sse, "bo-")
    plt.xlabel("Number of Clusters (k)")
    plt.ylabel("Sum of Squared Errors (SSE)")
    plt.title("Elbow Method For Optimal k")

    # Silhouette Coefficient Plot
    plt.subplot(1, 2, 2)
    plt.plot(k_range, silhouette_coefficients, "ro-")
    plt.xlabel("Number of Clusters (k)")
    plt.ylabel("Silhouette Coefficient")
    plt.title("Silhouette Coefficients For Optimal k")

    plt.tight_layout()
    plt.show()


# Finding Optimal Clusters
# def find_optimal_clusters(data, max_k):
#     iters = range(2, max_k + 1)
#     sse = []
#     silhouette_coefficients = []

#     for k in iters:
#         print("Fitting model with k={}".format(k))
#         model = KMeans(n_clusters=k, init="k-means++", random_state=42)
#         model.fit(data)
#         sse.append(model.inertia_)
#         score = silhouette_score(data, model.labels_)
#         silhouette_coefficients.append(score)

#     return sse, silhouette_coefficients


print("Vectorizing text...")
vectorizer = TfidfVectorizer()
X = vectorizer.fit_transform(df["text"])
print("Finished vectorizing text")

# print("Finding optimal clusters...")
# sse, silhouette_coefficients = find_optimal_clusters(X, 15)
# print("Finished finding optimal clusters")

# print("Plotting elbow method and silhouette coefficients...")
# plot_elbow_silhouette(sse, silhouette_coefficients, 15)
# print("Finished plotting elbow method and silhouette coefficients")

cluster_labels = knn_utils.knn_clustering(X)
knn_utils.plot_clusters(X, cluster_labels)
knn_utils.print_cluster_articles(df["text"], cluster_labels)


# optimal_clusters = 13
# kmeans = KMeans(n_clusters=optimal_clusters, init="k-means++", random_state=42)
# clusters = kmeans.fit_predict(X)

# # Adding cluster labels to your dataframe
# df["cluster"] = clusters

# for i in range(optimal_clusters):
#     cluster_data = df[df["cluster"] == i]
#     print(f"Cluster {i} characteristics:")
#     print(cluster_data.describe())

# feature_names = vectorizer.get_feature_names_out()
# top_n_words = 10

# # Print the top words for each cluster
# for i in range(optimal_clusters):
#     # Get the cluster center
#     cluster_center = kmeans.cluster_centers_[i]

#     # Sort the features (words) of the cluster center
#     sorted_features_index = cluster_center.argsort()[::-1]

#     # Get the top words for this cluster
#     top_words = [feature_names[ind] for ind in sorted_features_index[:top_n_words]]

#     print(f"Cluster {i} top words:")
#     print(", ".join(top_words))

# svd = TruncatedSVD(n_components=2)
# X_svd = svd.fit_transform(X)

# plt.figure(figsize=(10, 10))
# for i in range(optimal_clusters):
#     plt.scatter(X_svd[clusters == i, 0], X_svd[clusters == i, 1], label=f"Cluster {i}")
# plt.legend()
# plt.title("Cluster Visualization with TruncatedSVD")
# plt.xlabel("SVD Feature 1")
# plt.ylabel("SVD Feature 2")
# plt.show()
