pandas==2.0.3
numpy==1.26
scikit-learn==1.3
matplotlib==3.8.2
joblib==1.3.2