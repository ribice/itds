import pandas as pd
import numpy as np
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
from joblib import Parallel, delayed


def knn_clustering(data, n_neighbors=5, threshold=0.5):
    """
    Cluster data using a K-Nearest Neighbors approach with Scikit-learn and parallel processing.

    :param data: DataFrame, numpy array, or sparse matrix containing the preprocessed data.
    :param n_neighbors: The number of neighbors to use for kneighbors queries.
    :param threshold: The distance threshold for clustering.
    :return: A list of cluster labels for each data point.
    """

    print("Clustering using KNN with Scikit-learn and parallel processing...")

    # Initialize the NearestNeighbors model
    knn = NearestNeighbors(
        n_neighbors=n_neighbors + 1, metric="euclidean"
    )  # +1 because the point itself is included
    knn.fit(data)

    # Finding the nearest neighbors
    distances, indices = knn.kneighbors(data)

    # Helper function for parallel cluster assignment
    def assign_cluster(i):
        new_cluster = set()
        for j in range(1, n_neighbors + 1):  # Start from 1 to skip the point itself
            if distances[i, j] <= threshold:  # Check if within the threshold
                new_cluster.add(indices[i, j])
        return new_cluster

    # Parallel cluster assignment
    clusters = Parallel(n_jobs=-1)(
        delayed(assign_cluster)(i) for i in range(data.shape[0])
    )

    # Combine clusters
    final_clusters = []
    for cluster in clusters:
        if cluster:
            # Check if any element of the cluster is already in final_clusters
            if not any(
                cluster.intersection(existing_cluster)
                for existing_cluster in final_clusters
            ):
                final_clusters.append(cluster)

    # Convert clusters to labels for consistency
    cluster_labels = [-1] * data.shape[0]
    for cluster_id, cluster in enumerate(final_clusters):
        for point in cluster:
            cluster_labels[point] = cluster_id

    return cluster_labels


def plot_clusters(data, cluster_labels):
    """
    Plot the clusters using a 2D scatter plot.

    :param data: The high-dimensional data matrix.
    :param cluster_labels: The list of cluster labels for each data point.
    """

    print("Plotting clusters...")

    # Reducing the dimensionality for visualization
    svd = TruncatedSVD(n_components=2)
    data_2d = svd.fit_transform(data)

    # Plotting
    plt.figure(figsize=(10, 8))
    plt.scatter(
        data_2d[:, 0], data_2d[:, 1], c=cluster_labels, cmap="viridis", alpha=0.5
    )
    plt.colorbar(label="Cluster Label")
    plt.xlabel("Component 1")
    plt.ylabel("Component 2")
    plt.title("2D Visualization of Clusters")
    plt.show()


def print_cluster_articles(article_data, cluster_labels, num_articles=10):
    """
    Print a few articles from each cluster.

    :param article_data: The original dataset with article texts.
    :param cluster_labels: The list of cluster labels for each data point.
    :param num_articles: Number of articles to print from each cluster.
    """

    print("Printing cluster articles...")
    unique_clusters = set(cluster_labels)
    for cluster in unique_clusters:
        print(f"\nCluster {cluster}:\n")
        printed = 0
        for i, label in enumerate(cluster_labels):
            if label == cluster and printed < num_articles:
                # Assuming each row in article_data is an article
                print(article_data.iloc[i])
                printed += 1
                if printed >= num_articles:
                    break
